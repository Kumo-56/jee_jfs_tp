package com.example.jee_jsf.dao;

import com.example.jee_jsf.exception.DaoException;
import com.example.jee_jsf.model.User;

import java.util.List;

public interface UserDao {

	User getByUsername(String username) throws DaoException;

	List<User> getAll() throws DaoException;

	void createUser(User user) throws DaoException;
}
