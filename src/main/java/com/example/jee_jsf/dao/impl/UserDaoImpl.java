package com.example.jee_jsf.dao.impl;

import com.example.jee_jsf.dao.UserDao;
import com.example.jee_jsf.exception.DaoException;
import com.example.jee_jsf.model.User;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;

public class UserDaoImpl implements UserDao {

    private EntityManager em;

    public UserDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public User getByUsername(String username) throws DaoException {
        return null;
    }

    @Override
    public List<User> getAll() throws DaoException {
        try {
            TypedQuery<User> query = em.createQuery("SELECT u from User u order by u.id", User.class);
            return query.getResultList();
        } catch (IllegalArgumentException e) {
            throw new DaoException("Droit du user inconnu: " + e.getMessage());
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void createUser(User user) throws DaoException {
        try {
            em.persist(user);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
}
