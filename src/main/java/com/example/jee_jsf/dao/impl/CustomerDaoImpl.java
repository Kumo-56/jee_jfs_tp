package com.example.jee_jsf.dao.impl;

import com.example.jee_jsf.dao.CustomerDao;
import com.example.jee_jsf.exception.DaoException;
import com.example.jee_jsf.model.Customer;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;

public class CustomerDaoImpl implements CustomerDao {
	private EntityManager em;

	public CustomerDaoImpl(EntityManager em) {
		this.em = em;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> getAllCustomers() throws DaoException {
		try {
			TypedQuery<Customer> query = em.createQuery("SELECT c from Customer c order by c.id", Customer.class);
			return query.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	@Override
	public Customer getCustomerById(Integer id) throws DaoException {
		try {
			return em.find(Customer.class, id);
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void createCustomer(Customer customer) throws DaoException {
		try {
			em.persist(customer);
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void deleteCustomer(Customer customer) throws DaoException {
		try {
			em.remove(em.merge(customer));
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}
}
