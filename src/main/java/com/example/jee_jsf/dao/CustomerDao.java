package com.example.jee_jsf.dao;

import com.example.jee_jsf.exception.DaoException;
import com.example.jee_jsf.model.Customer;

import java.util.List;

public interface CustomerDao {

	Customer getCustomerById(Integer id) throws DaoException;
	List<Customer> getAllCustomers() throws DaoException;
	void createCustomer(Customer customer) throws DaoException;
	void deleteCustomer(Customer customer) throws DaoException;
}
