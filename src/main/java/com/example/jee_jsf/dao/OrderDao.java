package com.example.jee_jsf.dao;

import com.example.jee_jsf.exception.DaoException;
import com.example.jee_jsf.model.Order;

import java.util.List;

public interface OrderDao {

	Order getOrderById(Integer id) throws DaoException;

	Order getByLabel(String label) throws DaoException;

	List<Order> getAll() throws DaoException;

	void createOrder(Order order) throws DaoException;
}
