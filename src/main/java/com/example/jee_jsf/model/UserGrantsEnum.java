package com.example.jee_jsf.model;

public enum UserGrantsEnum {
    ADMIN,
    USER
}
