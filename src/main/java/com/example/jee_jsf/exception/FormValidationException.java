package com.example.jee_jsf.exception;

public class FormValidationException extends Exception {
    
	public FormValidationException( String message ) {
        super( message );
    }
}
