CREATE TABLE customers(id SERIAL PRIMARY KEY NOT NULL,
								lastname VARCHAR(100), 
                                firstname VARCHAR(100),
                                company VARCHAR(200),
                                mail VARCHAR(255),
                                phone VARCHAR(15),
                                mobile VARCHAR(15),
                                notes VARCHAR(255),
                                active BOOLEAN);

                                    
CREATE TABLE orders(id SERIAL PRIMARY KEY NOT NULL,
									customer_id INT,
                                    label VARCHAR(100),
                                    adr_et DECIMAL,
                                    number_of_days DECIMAL,
                                    tva DECIMAL,
                                    status VARCHAR(30),
                                    type VARCHAR(100),
                                    notes VARCHAR(255),
                                    FOREIGN KEY (customer_id) REFERENCES customers(id));                                    

CREATE TABLE users (id SERIAL PRIMARY KEY NOT NULL,
									   username VARCHAR(30),
                                       password VARCHAR(255),
                                       mail VARCHAR(255),
                                       grants VARCHAR(255));
